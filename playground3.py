import cntk as C
import numpy as np

from cntk import Constant
from cntk.learners import adadelta, learning_rate_schedule, UnitType, adam, momentum_schedule
from cntk.layers import Dense, Sequential, For, Dropout, Label, Tensor
from cntk.ops import relu, elu
from cntk.default_options import is_default_override, get_default_override, default_override_or
from cntk.layers.blocks import _initializer_for, _INFERRED, identity, UntestedBranchError  # helpers
from cntk.debugging import debug_model
from cntk.logging import TensorBoardProgressWriter
from cntk.internal import _as_tuple
from cntk.default_options import is_default_override, get_default_override, default_override_or
from cntk.variables import Parameter, Record, Constant
from cntk.ops.functions import Function, BlockFunction
from cntk.layers import *
from cntk.layers.sequence import *
from cntk.layers.models.attention import *
from cntk.layers.typing import *

#X = np.matrix('0 0 0; 0 1 0; 1 0 0; 1 1 0; 0 0 1; 0 1 1; 1 0 1; 1 1 1; 0 0 2; 0 1 2; 1 0 2; 1 1 2; 0 0 3; 0 1 3; 1 0 3; 1 1 3', dtype=np.float32)
X = np.matrix('0 0 0 1; 0 1 0 1; 1 0 0 1; 1 1 0 1; 0 0 1 2; 0 1 1 2; 1 0 1 2; 1 1 1 2; 0 0 2 3; 0 1 2 3; 1 0 2 3; 1 1 2 3; 0 0 3 0; 0 1 3 0; 1 0 3 0; 1 1 3 0', dtype=np.float32)
Y = np.matrix('0; 1; 1; 0; 1; 0; 0; 1; 1; 1; 0; 0; 0; 0; 1; 1', dtype=np.float32)
X = np.arange(400, dtype=np.float32).reshape((-1,1))
Y = np.sin(np.radians(X))
#Y = np.minimum(Yt, 360-Yt)

input_dim = 1
output_dim = 1
num_hiddel_layer = 1
hidden_layers_dim = 4
num_epochs = 10000
slices = 4

def meaningful_squared_error(z, label):
    tz = C.element_select(C.less(z, 0), z+1.0, z)
    tl = C.element_select(C.less(label, 0), label+1.0, label)
    t = C.abs(tz-tl)
    t = C.element_min(t, 1.0-t)
    return t*t

if __name__=='__main__':
    input_val = C.input_variable(input_dim, np.float32)
    label_val = C.input_variable(output_dim, np.float32)

    model = Sequential([
        Dense((input_dim, hidden_layers_dim), activation=elu),
        Dense((hidden_layers_dim, hidden_layers_dim), activation=elu),
        Dense((hidden_layers_dim, hidden_layers_dim), activation=elu),
        Dense((hidden_layers_dim, hidden_layers_dim), activation=elu),
        Dense(output_dim)
    ])(input_val)
    
    loss = C.losses.squared_error(model, label_val)
    #loss = meaningful_squared_error(model, label_val)
    metric = C.reduce_mean(C.abs(model(input_val) - label_val))
    #metric = C.metrics.classification_error(model, label)
    #criterion = C.combine([loss, metric])
    criterion = (loss, metric)
    progress_writer = [C.logging.ProgressPrinter(
        tag = "Training",
        num_epochs = num_epochs
    )]
    tensorboard_writer = TensorBoardProgressWriter(freq=10, log_dir='log', model=model)

    #lr = learning_rate_schedule(1, UnitType.sample)
    #lr = learning_rate_schedule(0.01, UnitType.minibatch)
    #learner = C.learners.sgd(model.parameters, lr)
    lr_schedule = learning_rate_schedule(0.001, UnitType.minibatch)
    m_schedule = momentum_schedule(0.9)
    vm_schedule = momentum_schedule(0.999)
    learner = adam(model.parameters, lr_schedule, momentum=m_schedule, 
                                                  variance_momentum=vm_schedule)
    trainer = C.Trainer(model, criterion, [learner], progress_writers=progress_writer)

    print(X, Y, input_val.shape)

    for i in range(num_epochs):
        #trainer.train_minibatch({input_val: X, label_val: Y})
        s = np.arange(X.shape[0])
        np.random.shuffle(s)
        minibatch_size = 32
        for i in range(0, X.shape[0], minibatch_size):
            trainer.train_minibatch({input_val: X[s][i:i+minibatch_size], label_val: Y[s][i:i+minibatch_size]})
        trainer.summarize_training_progress()
        if i % 20 == 0:
            print(model.eval(X[0]))

    #Xt = [[359.7],[360.0],[360.3],[360.5],[360.8],[361.0]]
    Xt = [[-0.7],[-0.5],[-0.3],[-1.0],[-1.3],[-1.5]]
    print('xor\n', np.concatenate([model.eval(X),Y], axis=-1), model.eval(Xt))

    model.save('sine.cntk1')
