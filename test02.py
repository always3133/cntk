from __future__ import print_function
import cntk
import numpy as np
import scipy.sparse

# Define a dictionary to store the model parameters
mydict = {}

def linear_layer(input_var, output_dim):
    input_dim = input_var.shape[0]
    weight_param = cntk.parameter(shape=(input_dim, output_dim))
    bias_param = cntk.parameter(shape=(output_dim))
    
    mydict['w'], mydict['b'] = weight_param, bias_param

    return cntk.times(input_var, weight_param) + bias_param

# Define the network
input_dim = 2
num_output_classes = 2

# Ensure we always get the same amount of randomness
np.random.seed(0)

# Helper function to generate a random data sample
def generate_random_data_sample(sample_size, feature_dim, num_classes):
    # Create synthetic data using NumPy. 
    Y = np.random.randint(size=(sample_size, 1), low=0, high=num_classes)

    # Make sure that the data is separable 
    X = (np.random.randn(sample_size, feature_dim)+3) * (Y+1)
    
    # Specify the data type to match the input variable used later in the tutorial 
    # (default type is double)
    X = X.astype(np.float32)    
    
    # converting class 0 into the vector "1 0 0", 
    # class 1 into vector "0 1 0", ...
    class_ind = [Y==class_number for class_number in range(num_classes)]
    Y = np.asarray(np.hstack(class_ind), dtype=np.float32)
    return X, Y

# Create the input variables denoting the features and the label data. Note: the input 
# does not need additional info on number of observations (Samples) since CNTK creates only 
# the network topology first 
mysamplesize = 32
features, labels = generate_random_data_sample(mysamplesize, input_dim, num_output_classes)

feature = cntk.input_variable(input_dim, np.float32)

output_dim = num_output_classes
z = linear_layer(feature, output_dim)

label = cntk.input_variable((num_output_classes), np.float32)
loss = cntk.cross_entropy_with_softmax(z, label)

eval_error = cntk.classification_error(z, label)

# Instantiate the trainer object to drive the model training
learning_rate = 0.5
lr_schedule = cntk.learning_rate_schedule(learning_rate, cntk.UnitType.minibatch) 
learner = cntk.sgd(z.parameters, lr_schedule)
trainer = cntk.Trainer(z, (loss, eval_error), [learner])
