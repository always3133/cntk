import cntk
import numpy as np
from cntk.ops.functions import load_model

feature = cntk.input_variable(2, np.float32)

model0 = load_model('xor.pretrained')
print(np.around(model0.eval(np.matrix('0 0; 1 0; 0 1; 1 1', dtype=np.float32))))

model1 = load_model('xnor.pretrained')
print(np.around(model1.eval(np.matrix('0 0; 1 0; 0 1; 1 1', dtype=np.float32))))