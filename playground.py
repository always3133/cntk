import cntk as C
import numpy as np

from cntk import Constant
from cntk.learners import adadelta, learning_rate_schedule, UnitType
from cntk.layers import Dense, Sequential, For, Dropout, Label, Tensor
from cntk.ops import relu, elu
from cntk.default_options import is_default_override, get_default_override, default_override_or
from cntk.layers.blocks import _initializer_for, _INFERRED, identity, UntestedBranchError  # helpers
from cntk.debugging import debug_model
from cntk.logging import TensorBoardProgressWriter
from cntk.internal import _as_tuple
from cntk.default_options import is_default_override, get_default_override, default_override_or
from cntk.variables import Parameter, Record, Constant
from cntk.ops.functions import Function, BlockFunction
from cntk.layers import *
from cntk.layers.sequence import *
from cntk.layers.models.attention import *
from cntk.layers.typing import *

def Dense2(shape, activation=default_override_or(identity), init=default_override_or(C.glorot_uniform()),
          input_rank=None, map_rank=None,
          bias=default_override_or(True), init_bias=default_override_or(0),
          name='',
          slices=1,
          phase=0):

    activation = get_default_override(Dense, activation=activation)
    init       = get_default_override(Dense, init=init)
    bias       = get_default_override(Dense, bias=bias)
    init_bias  = get_default_override(Dense, init_bias=init_bias)

    output_shape = _as_tuple(shape)

    if input_rank is not None and map_rank is not None:
        raise ValueError("Dense: input_rank and map_rank cannot be specified at the same time.")

    # determine meaning of axes
    # W gets dimension (input_shape + shape)
    # where input_shape is determined as:
    #  - by default, equal to the dimensions of the input passed to Dense()
    #  - if input_rank is given, then the last 'input_rank' dimensions of the input (all others are not reduced over)
    #  - if map_rank is given, then the all but the first 'map_rank' dimensions of the input (those are not reduced over)
    # where input_rank and map_rank are mutuallly exclusive.

    output_rank = len(output_shape)   # support outputs with tensor layouts

    # If input_rank not given then pass a single _INFERRED; map_rank if given will determine the input_rank.
    # The dimension inference may still create multiple axes.
    input_shape = _INFERRED * (input_rank if input_rank is not None else 1)

    if input_rank is not None:
        infer_input_rank_to_map = -1 # means map_rank is not specified; input_rank rules
    elif map_rank is None:
        infer_input_rank_to_map = 0  # neither given: default to 'infer W to use all input dims'
    else:
        infer_input_rank_to_map = map_rank  # infer W to use all input dims except the first static 'map_rank' ones

    # parameters bound to this Function
    init_weights = _initializer_for(init, Record(output_rank=output_rank))
    Wp = Parameter((slices,) + shape, init=init_weights, name='W')
    bp = Parameter((slices,) + (shape[-1],), init=init_bias,    name='b') if bias else None

    # expression of this function
    @BlockFunction('Dense2', name)
    #@Signature(Tensor[input_dim])
    def dense2(x):
        p = C.reshape(x[-1], (1,))
        eq = C.equal(p, 0)
        Wx = C.element_select(eq, Wp[0], Wp[1])
        bx = C.element_select(eq, bp[0], bp[1])

        Wx = Wp[0]
        bx = bp[0]

        W = C.reshape(Wx, shape)
        b = C.reshape(bx, (shape[-1],))

        print('w', x.shape, p.shape, eq.shape, shape, Wp.shape, W.shape)
        print('b', x.shape, p.shape, eq.shape, shape, bp.shape, b.shape)

        r = C.times(x[:-1], W, output_rank=output_rank, infer_input_rank_to_map=infer_input_rank_to_map)
        if b:
            r = r + b
        if activation is not None:
            r = activation(r)
        return r
    return dense2

#X = np.matrix('0 0 0; 0 1 0; 1 0 0; 1 1 0; 0 0 1; 0 1 1; 1 0 1; 1 1 1; 0 0 2; 0 1 2; 1 0 2; 1 1 2; 0 0 3; 0 1 3; 1 0 3; 1 1 3', dtype=np.float32)
X = np.matrix('0 0 0 1; 0 1 0 1; 1 0 0 1; 1 1 0 1; 0 0 1 2; 0 1 1 2; 1 0 1 2; 1 1 1 2; 0 0 2 3; 0 1 2 3; 1 0 2 3; 1 1 2 3; 0 0 3 0; 0 1 3 0; 1 0 3 0; 1 1 3 0', dtype=np.float32)
Y = np.matrix('0; 1; 1; 0; 1; 0; 0; 1; 1; 1; 0; 0; 0; 0; 1; 1', dtype=np.float32)

# X = np.matrix('0 0 0; 0 1 0; 1 0 0; 1 1 0', dtype=np.float32)
# Y = np.matrix('0; 1; 1; 0', dtype=np.float32)

# X = np.matrix('0 0 1; 0 1 1; 1 0 1; 1 1 1', dtype=np.float32)
# Y = np.matrix('1; 0; 0; 1', dtype=np.float32)

# X = np.matrix('0 0 0; 0 1 0; 1 0 0; 1 1 0; 0 0 0; 0 1 0; 1 0 0; 1 1 0', dtype=np.float32)
# Y = np.matrix('0; 1; 1; 0; 1; 0; 0; 1', dtype=np.float32)

X0 = np.matrix('0 0; 0 1; 1 0; 1 1', dtype=np.float32)
Y0 = np.matrix('0; 1; 1; 0', dtype=np.float32)

X1 = np.matrix('0 0; 0 1; 1 0; 1 1', dtype=np.float32)
Y1 = np.matrix('1; 0; 0; 1', dtype=np.float32)

# X = np.matrix('0 0; 0 1; 1 0; 1 1', dtype=np.float32)
# Y = np.matrix('0; 1; 1; 0', dtype=np.float32)

input_dim = 2
output_dim = 1
num_hiddel_layer = 1
hidden_layers_dim = 32
num_epochs = 10

feature = C.input_variable(input_dim+2, np.float32)
label = C.input_variable(output_dim, np.float32)

def create_model():
    #model = Sequential([For(range(num_hiddel_layer), lambda i: Dense(hidden_layers_dim, activation=relu)), Dense(1)])(feature)
    # model = Sequential([
    #     Dense2((input_dim+1, hidden_layers_dim), activation=relu, slices=2, phase=p),
    #     Dense2((hidden_layers_dim, hidden_layers_dim), activation=relu, slices=2, phase=p),
    #     Dense(output_dim)
    # ])(feature)

    # model = Sequential([
    #     Dense(hidden_layers_dim, activation=relu),
    #     Dense(hidden_layers_dim, activation=relu),
    #     Dense(output_dim)
    # ])(feature)

    #phase = C.input_variable((1,), np.float32, name='phase')
    # print(phase)

    class TLayer:
        def __init__(self, shape, slices=1, activation=identity, init_weight=C.glorot_normal(), init_bias=0):
            self.W = C.Parameter((slices,)+shape, init=init_weight, name='W')
            self.b = C.Parameter((slices,)+(shape[-1],), init=init_bias, name='b')
            self.activation = activation
            self.shape = shape
            self.slices = slices
            self.phases = C.Parameter((1,), init=0., name='phases')

        def bypass(self, input):
            return input

        def invoke(self, input, phase=0, phase2=0):
            eq0 = C.equal(phase, 0.)
            eq1 = C.equal(phase, 1.)
            eq2 = C.equal(phase, 2.)
            eq3 = C.equal(phase, 3.)
            # Wx = C.element_select(eq, self.W[0], np.  zeros(self.shape))
            # bx = C.element_select(eq, self.b[0], np.zeros((self.shape[-1],)))
            Wx = C.element_select(eq0, self.W[0],
                    C.element_select(eq1, self.W[1], 
                    C.element_select(eq2, self.W[2], self.W[3])))
                                                                                                                 
            bx = C.element_select(eq0, self.b[0],
                    C.element_select(eq1, self.b[1], 
                    C.element_select(eq2, self.b[2], self.b[3])))

            # if eq == False:
            #     eq = C.equal(phase, 1)
            #     Wx = C.element_select(eq, self.W[1], np.zeros(self.shape))
            #     bx = C.element_select(eq, self.b[1], np.zeros((self.shape[-1],)))

            # Wx = self.W[0]
            # bx = self.b[0]

            # Wx = C.plus(self.W[0], C.gather(self.W, phase))
            # bx = C.plus(self.b[0], C.gather(self.b, phase))

            # Wx = C.plus(C.gather(self.W, phase), C.gather(self.W, phase2))
            # bx = C.plus(C.gather(self.b, phase), C.gather(self.b, phase2))

            phase2 = C.plus(phase, 1)
            phase2 = C.element_select(C.greater_equal(phase2, 2), 0, phase2)

            W1 = C.alias(C.gather(self.W, phase))
            W2 = C.alias(C.gather(self.W, phase2))

            b1 = C.alias(C.gather(self.b, phase))
            b2 = C.alias(C.gather(self.b, phase2))

            Wx = W1 + W2
            bx = b1 + b2
            #bx = C.gather(self.b, phase2) + self.b[0]
            #bx = self.b[0]

            # print('a', Wx, Wx.shape)            
            # #Wx =  C.gather(self.W, phase) + C.gather(self.W, phase2)
            # Wx =  C.gather(self.W, phase2)*3
            # print('b', Wx, Wx.shape)            

            # Wx = C.gather(self.W, phase)
            # Bx = C.gather(self.b, phase)

            W = C.reshape(Wx, self.shape)
            b = C.reshape(bx, self.shape[-1])
            # W = C.reshape(self.W[0], self.shape)
            # b = C.reshape(self.b[0], self.shape[-1])
            #print('q', self.W[0].shape, self.b[0].shape, 'p', Wx.shape, bx.shape, 't', W.shape, b.shape)
            
            return self.activation(C.times(input, W) + b)

        # def invoke2(self, input, phase):
        #     @Function
        #     def invoke2(input):
        #         if phase == [0]:
        #             Wx = self.W[0]
        #             bx = self.b[0]
        #         else:
        #             Wx = self.W[1]
        #             bx = self.b[1]

        #         W = C.reshape(Wx, self.shape)
        #         b = C.reshape(bx, self.shape[-1])

        #         return self.activation(C.times(input, W) + b)
            
        #     return invoke2

    l1 = TLayer((input_dim, hidden_layers_dim),         slices=4, activation=relu)
    l2 = TLayer((hidden_layers_dim, hidden_layers_dim), slices=4, activation=relu)
    l3 = TLayer((hidden_layers_dim, 2),                 slices=4)
    #l4 = TLayer((2, output_dim),                        slices=4)
    
    p1 = feature[-2]
    p2 = feature[-1]
    m1 = l1.invoke(feature[:-2], phase=p1, phase2=p2)
    m2 = l2.invoke(m1,           phase=p1, phase2=p2)
    m3 = l3.invoke(m2,           phase=p1, phase2=p2)
    #m4 = l4.bypass(m3)

    model = m3

    #print(feature[-1].eval(np.matrix('0 0 1')))

    # W = C.Parameter((4, input_dim, hidden_layers_dim), init=C.glorot_uniform(), name='W')
    # b = C.Parameter((4, hidden_layers_dim,), init=0, name='b')

    # c = np.matrix('1 0 0 0', dtype=np.float32)
    # Wp = C.reshape(W[0], (input_dim, hidden_layers_dim))
    # bp = C.reshape(b[0], (hidden_layers_dim,))
    # layer1 = C.relu(C.times(feature, Wp) + bp)

    # W1 = C.Parameter((hidden_layers_dim, hidden_layers_dim), init=C.glorot_uniform(), name='W')
    # b1 = C.Parameter((hidden_layers_dim,), init=0, name='b')
    # layer2 = C.relu(C.times(layer1, W1) + b1)

    # W2 = C.Parameter((hidden_layers_dim, output_dim), init=C.glorot_uniform(), name='W')
    # b2 = C.Parameter((output_dim,), init=0, name='b')
    # layer2 = C.times(layer2, W2) + b2
    # model = layer2

    loss = C.losses.squared_error(model, label)
    metric = np.square(model(feature) - label)
    #metric = C.metrics.classification_error(model, label)
    #criterion = C.combine([loss, metric])
    criterion = loss
    progress_writer = [C.logging.ProgressPrinter(
        tag = "Training",
        num_epochs = num_epochs
    )]
    tensorboard_writer = TensorBoardProgressWriter(freq=10, log_dir='log', model=model)

    #lr = learning_rate_schedule(1, UnitType.sample)
    lr = learning_rate_schedule(0.1, UnitType.minibatch)
    learner = C.learners.sgd(model.parameters, lr)
    trainer = C.Trainer(model, criterion, [learner], progress_writers=progress_writer)
    return trainer, model, l1, l2, l3

def peek(model, epoch):
    pass

def train(input, output, model, max_epochs):
    pass

# xor trainer
trainer0, model0, l1, l2, l3 = create_model()

for i in range(num_epochs):
    trainer0.train_minibatch({feature: X, label: Y})#, phase: np.full((1,), 1.)})
    trainer0.summarize_training_progress()
    if i % 20 == 0:
        print(model0.eval(X))

# xnor trainer
# trainer1, model1 = create_model()
# for i in range(num_epochs):
#     trainer1.train_minibatch({feature: X1, label: Y1})#, phase: np.full((1,), 1.)})
#     trainer1.summarize_training_progress()
#     if i % 20 == 0:
#         print(model1.eval(X1))
        
print('xor\n', np.around(model0.eval(X)))
# print('xnor\n', np.around(model1.eval(X1)))

# model0.save('xor.pretrained')
# model1.save('xnor.pretrained')
# print(W.value)
# print(W1.value)
# print(phase)
# print(l3.W.value)
print(l1.b.value)
print(l2.b.value)
#print(l3.W.value)
print(l3.b.value)

print(l3.phases)
# a = np.ones((2, 3), dtype=np.float32)
# #c = C.element_select(np.matrix('1 0 0; 0 0 0'), a, np.zeros((2, 3)))
# c = C.element_select(False, a, np.zeros((2, 3)))
# print(c.eval())

d = np.array([1,2,3,4], dtype=np.float32)
d = np.arange(15, dtype=np.float32)
y = np.array([0,0,0,1], dtype=np.float32)
a = C.sequence.input_variable(shape=(1), dtype=np.float32, name='a')
a_last = C.sequence.is_last(a)

an = C.floor(a[-1])
eqs = C.one_hot(an, 4, False)
#eqs = C.reshape(eqs, (1,-1,1))
print(type(eqs), a.shape, eqs.shape)
l = C.sequence.gather(a, a_last)
print(l.eval({a:d.reshape(1,-1,1)}))
print(a_last.eval({a:d.reshape(1,-1,1)}))
print(eqs.eval({a:d.reshape(1,-1,1)}))
print(an.eval({a:d.reshape(1,-1,1)}))

#######
xx = C.sequence.input_variable(shape=(1))
yy = C.sequence.input_variable(shape=(1))
zz = C.sequence.gather(xx, yy)

xx_value = np.arange(15, dtype=np.float32)
yy_value = np.array([1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1], dtype=np.float32)
aa = zz.eval({xx: xx_value.reshape(1, -1, 1), yy: yy_value.reshape(1, -1, 1)})
print(aa)


# a = C.sequence.input_variable(shape=(3,4),
#                     dtype=np.float32,
#                     name='a')

# is_last_a = C.sequence.is_last(a)
# a_last = C.sequence.gather(a, is_last_a)