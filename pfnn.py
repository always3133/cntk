import cntk as C
import numpy as np
import os,argparse
from datetime import datetime

from cntk import Constant, Trainer
from cntk.learners import adadelta, learning_rate_schedule, UnitType
from cntk.layers import Dense, Sequential, For, Dropout, Tensor
from cntk.ops import relu, elu, dropout
from cntk.default_options import is_default_override, get_default_override, default_override_or
from cntk.layers.blocks import _initializer_for, _INFERRED, identity, UntestedBranchError  # helpers
from cntk.learners import adam, learning_rate_schedule, momentum_schedule, UnitType
from cntk.logging import *
from cntk.debugging import *

def load_data(dbpath):
    database = np.load(dbpath)
    X = database['Xun'].astype(np.float32)
    Y = database['Yun'].astype(np.float32)
    P = database['Pun'].astype(np.float32)

    Xmean, Xstd = X.mean(axis=0), X.std(axis=0)
    Ymean, Ystd = Y.mean(axis=0), Y.std(axis=0)

    j = 31
    w = ((60*2)//10)

    Xstd[w*0:w* 1] = Xstd[w*0:w* 1].mean() # Trajectory Past Positions
    Xstd[w*1:w* 2] = Xstd[w*1:w* 2].mean() # Trajectory Future Positions
    Xstd[w*2:w* 3] = Xstd[w*2:w* 3].mean() # Trajectory Past Directions
    Xstd[w*3:w* 4] = Xstd[w*3:w* 4].mean() # Trajectory Future Directions
    Xstd[w*4:w*10] = Xstd[w*4:w*10].mean() # Trajectory Gait

    """ Mask Out Unused Joints in Input """
    joint_weights = np.array([
        1,
        1e-10, 1, 1, 1, 1,
        1e-10, 1, 1, 1, 1,
        1e-10, 1, 1,
        1e-10, 1, 1,
        1e-10, 1, 1, 1, 1e-10, 1e-10, 1e-10,
        1e-10, 1, 1, 1, 1e-10, 1e-10, 1e-10]).repeat(3)

    Xstd[w*10+j*3*0:w*10+j*3*1] = Xstd[w*10+j*3*0:w*10+j*3*1].mean() / (joint_weights * 0.1) # Pos
    Xstd[w*10+j*3*1:w*10+j*3*2] = Xstd[w*10+j*3*1:w*10+j*3*2].mean() / (joint_weights * 0.1) # Vel
    Xstd[w*10+j*3*2:          ] = Xstd[w*10+j*3*2:          ].mean() # Terrain

    Ystd[0:2] = Ystd[0:2].mean() # Translational Velocity
    Ystd[2:3] = Ystd[2:3].mean() # Rotational Velocity
    Ystd[3:4] = Ystd[3:4].mean() # Change in Phase
    Ystd[4:8] = Ystd[4:8].mean() # Contacts

    Ystd[8+w*0:8+w*1] = Ystd[8+w*0:8+w*1].mean() # Trajectory Future Positions
    Ystd[8+w*1:8+w*2] = Ystd[8+w*1:8+w*2].mean() # Trajectory Future Directions

    Ystd[8+w*2+j*3*0:8+w*2+j*3*1] = Ystd[8+w*2+j*3*0:8+w*2+j*3*1].mean() # Pos
    Ystd[8+w*2+j*3*1:8+w*2+j*3*2] = Ystd[8+w*2+j*3*1:8+w*2+j*3*2].mean() # Vel
    Ystd[8+w*2+j*3*2:8+w*2+j*3*3] = Ystd[8+w*2+j*3*2:8+w*2+j*3*3].mean() # Rot

    """ Save Mean / Std / Min / Max """
    # Xmean.astype(np.float32).tofile('./network/Xmean.bin')
    # Ymean.astype(np.float32).tofile('./network/Ymean.bin')
    # Xstd.astype(np.float32).tofile('./network/Xstd.bin')
    # Ystd.astype(np.float32).tofile('./network/Ystd.bin')

    """ Normalize Data """
    X = (X - Xmean) / Xstd
    Y = (Y - Ymean) / Ystd

    print('X', X.shape, Xstd.shape)
    print('Y', Y.shape, Ystd.shape)

    return X, Y, P

input_dim = 342
output_dim = 311
hidden_layers_dim = 512

########################
# define the model     #
########################
def create_model_function_legacy():
    return Sequential([
        Dropout(0.7),
        Dense(hidden_layers_dim, activation=elu),
        Dropout(0.7),
        Dense(hidden_layers_dim, activation=elu),
        Dropout(0.7),
        Dense(output_dim)
    ])

@C.Function
def cubic(y0, y1, y2, y3, mu):
    return (
        (-0.5*y0+1.5*y1-1.5*y2+0.5*y3)*mu*mu*mu + 
        (y0-2.5*y1+2.0*y2-0.5*y3)*mu*mu + 
        (-0.5*y0+0.5*y2)*mu +
        (y1))

@C.Function
def linear(m0, m1, alpha):
    return (m0 * alpha) + (m1 * (1-alpha))

@C.Function
def weight(w0, w1, w2, w3, p):
    pn = C.floor(p)
    px = p-pn
    if pn == C.constant(0.):
        return w3, w0, w1, w2, px
    elif pn == C.constant(1.):
        return w0, w1, w2, w3, px
    elif pn == C.constant(2.):
        return w1, w2, w3, w0, px
    else:
        return w2, w3, w0, w1, px

@C.Function
def bias(b0, b1, b2, b3, p):
    pn = C.floor(p)
    px = p-pn
    if pn == C.constant(0.):
        return b3, b0, b1, b2, px
    elif pn == C.constant(1.):
        return b0, b1, b2, b3, px
    elif pn == C.constant(2.):
        return b1, b2, b3, b0, px
    else:
        return b2, b3, b0, b1, px

def create_model_function(input_var, network_type):
    class TLayer:
        def __init__(self, shape, slices=1, activation=identity, init_weight=C.glorot_normal(), init_bias=0):
            self.W = C.Parameter((slices,)+shape, init=init_weight, name='W')
            self.b = C.Parameter((slices,)+(shape[-1],), init=init_bias, name='b')
            self.activation = activation
            self.shape = shape
            self.slices = slices

        def invoke_cubic(self, input, raw=0.0):
            phase = C.floor(raw)
            phasex = raw-phase

            eq0 = C.equal(phase, 0.)
            eq1 = C.equal(phase, 1.)
            eq2 = C.equal(phase, 2.)
            eq3 = C.equal(phase, 3.)

            W0 = C.element_select(eq0, self.W[3],
                    C.element_select(eq1, self.W[0], 
                    C.element_select(eq2, self.W[1], self.W[2])))
            W1 = C.element_select(eq0, self.W[0],
                    C.element_select(eq1, self.W[1], 
                    C.element_select(eq2, self.W[2], self.W[3])))
            W2 = C.element_select(eq0, self.W[1],
                    C.element_select(eq1, self.W[2], 
                    C.element_select(eq2, self.W[3], self.W[0])))
            W3 = C.element_select(eq0, self.W[2],
                    C.element_select(eq1, self.W[3], 
                    C.element_select(eq2, self.W[0], self.W[1])))

            b0 = C.element_select(eq0, self.b[3],
                    C.element_select(eq1, self.b[0], 
                    C.element_select(eq2, self.b[1], self.b[2])))
            b1 = C.element_select(eq0, self.b[0],
                    C.element_select(eq1, self.b[1], 
                    C.element_select(eq2, self.b[2], self.b[3])))
            b2 = C.element_select(eq0, self.b[1],
                    C.element_select(eq1, self.b[2], 
                    C.element_select(eq2, self.b[3], self.b[0])))
            b3 = C.element_select(eq0, self.b[2],
                    C.element_select(eq1, self.b[3], 
                    C.element_select(eq2, self.b[0], self.b[1])))

            # Wx = C.element_select(eq0, self.W[0],
            #         C.element_select(eq1, self.W[1], 
            #         C.element_select(eq2, self.W[2], self.W[3])))
                                                                                                                 
            # bx = C.element_select(eq0, self.b[0],
            #         C.element_select(eq1, self.b[1], 
            #         C.element_select(eq2, self.b[2], self.b[3])))

            Wx = cubic(W0, W1, W2, W3, phasex)
            bx = cubic(b0, b1, b2, b3, phasex)

            # Wx = linear(W1, W2, phasex)
            # bx = linear(b1, b2, phasex)

            W = C.reshape(Wx, self.shape)
            b = C.reshape(bx, self.shape[-1])
            
            #print(self.shape, W.shape, b.shape, input.shape)
            
            return self.activation(C.times(input, W) + b)

        def invoke_linear(self, input, raw=0.0):
            phase = C.floor(raw)
            phasex = raw-phase

            eq0 = C.equal(phase, 0.)
            eq1 = C.equal(phase, 1.)
            eq2 = C.equal(phase, 2.)
            eq3 = C.equal(phase, 3.)

            # W0 = C.element_select(eq0, self.W[3],
            #         C.element_select(eq1, self.W[0], 
            #         C.element_select(eq2, self.W[1], self.W[2])))
            W1 = C.element_select(eq0, self.W[0],
                    C.element_select(eq1, self.W[1], 
                    C.element_select(eq2, self.W[2], self.W[3])))
            W2 = C.element_select(eq0, self.W[1],
                    C.element_select(eq1, self.W[2], 
                    C.element_select(eq2, self.W[3], self.W[0])))
            # W3 = C.element_select(eq0, self.W[2],
            #         C.element_select(eq1, self.W[3], 
            #         C.element_select(eq2, self.W[0], self.W[1])))

            # b0 = C.element_select(eq0, self.b[3],
            #         C.element_select(eq1, self.b[0], 
            #         C.element_select(eq2, self.b[1], self.b[2])))
            b1 = C.element_select(eq0, self.b[0],
                    C.element_select(eq1, self.b[1], 
                    C.element_select(eq2, self.b[2], self.b[3])))
            b2 = C.element_select(eq0, self.b[1],
                    C.element_select(eq1, self.b[2], 
                    C.element_select(eq2, self.b[3], self.b[0])))
            # b3 = C.element_select(eq0, self.b[2],
            #         C.element_select(eq1, self.b[3], 
            #         C.element_select(eq2, self.b[0], self.b[1])))

            # Wx = C.element_select(eq0, self.W[0],
            #         C.element_select(eq1, self.W[1], 
            #         C.element_select(eq2, self.W[2], self.W[3])))
                                                                                                                 
            # bx = C.element_select(eq0, self.b[0],
            #         C.element_select(eq1, self.b[1], 
            #         C.element_select(eq2, self.b[2], self.b[3])))

            # W0, W1, W2, W3, phasex = weight(self.W[0], self.W[1], self.W[2], self.W[3], raw)
            # b0, b1, b2, b3, phasex = bias(self.b[0], self.b[1], self.b[2], self.b[3], raw)

            # Wx = cubic(W0, W1, W2, W3, phasex)
            # bx = cubic(b0, b1, b2, b3, phasex)

            Wx = linear(W1, W2, phasex)
            bx = linear(b1, b2, phasex)

            W = C.reshape(Wx, self.shape)
            b = C.reshape(bx, self.shape[-1])
            
            #print(self.shape, W.shape, b.shape, input.shape)
            
            return self.activation(C.times(input, W) + b)

        def invoke_constant(self, input, raw=0.0):
            phase = C.floor(raw)
            phasex = raw-phase

            eq0 = C.equal(phase, 0.)
            eq1 = C.equal(phase, 1.)
            eq2 = C.equal(phase, 2.)
            eq3 = C.equal(phase, 3.)

            Wx = C.element_select(eq0, self.W[0],
                    C.element_select(eq1, self.W[1], 
                    C.element_select(eq2, self.W[2], self.W[3])))
                                                                                                                 
            bx = C.element_select(eq0, self.b[0],
                    C.element_select(eq1, self.b[1], 
                    C.element_select(eq2, self.b[2], self.b[3])))

            W = C.reshape(Wx, self.shape)
            b = C.reshape(bx, self.shape[-1])
            
            return self.activation(C.times(input, W) + b)

    l1 = TLayer((input_dim, hidden_layers_dim),         slices=4, activation=elu)
    l2 = TLayer((hidden_layers_dim, hidden_layers_dim), slices=4, activation=elu)
    l3 = TLayer((hidden_layers_dim, output_dim),        slices=4)

    x = input_var[:-1]
    p = input_var[-1]

    x = dropout(x, 0.7)
    x = l1.invoke_linear(x, raw=p) if network_type == 'linear' else l1.invoke_cubic(x, raw=p) if network_type == 'cubic' else l1.invoke_constant(x, raw=p)
    x = dropout(x, 0.7)
    x = l2.invoke_linear(x, raw=p) if network_type == 'linear' else l2.invoke_cubic(x, raw=p) if network_type == 'cubic' else l2.invoke_constant(x, raw=p)
    x = dropout(x, 0.7)
    x = l3.invoke_linear(x, raw=p) if network_type == 'linear' else l3.invoke_cubic(x, raw=p) if network_type == 'cubic' else l3.invoke_constant(x, raw=p)

    model = x
    return model

########################
# define the criteria  #
########################
# compose model function and criterion primitives into a criterion function
#  takes:   Function: features -> prediction
#  returns: Function: (features, labels) -> (loss, metric)
def create_criterion_function(model):
    @C.Function.with_signature(input = Tensor[input_dim+1], label = Tensor[output_dim])
    def criterion(input, label):
        z = model(input)
        ce = C.losses.squared_error(z, label)
        #errs = cntk.metrics.classification_error(z, labels)
        #return (ce, errs)
        return ce
    return criterion

def peek(model, input, output, epoch, network_type, tensorboard_writer=None, model_dir=None, log_dir=None):
    with open(log_dir, 'a') as file:
        for i in range(25050, 25070):
            file.write("\t peek input {}, loss: {}\n".format(i, C.squared_error(model(input[i]), output[i]).eval()))

    if tensorboard_writer:
        for parameter in model.parameters:
            tensorboard_writer.write_value(parameter.uid + "/mean", C.reduce_mean(parameter).eval(), epoch)
    if model_dir:
        model.save(os.path.join(model_dir, network_type + "_{}.pfnn".format(epoch)))

def train(input, output, max_epochs, network_type, minibatch_size, 
            unit_type, learning_rate,
            l1r=0.0, l2r=0.0, profiler_dir=None, output_dir=None, log_dir=None, tensorboard_logdir=None, gen_heartbeat=False):
    input_var = C.input_variable(input_dim+1, np.float32)
    label_var = C.input_variable(output_dim, np.float32)

    model = create_model_function(input_var, network_type)
    
    sample_size = input.shape[0]
    epoch_size = sample_size
    #minibatch_size = sample_size//900

    criterion = create_criterion_function(model)

    lr_schedule = learning_rate_schedule(learning_rate, unit_type)
    m_schedule = momentum_schedule(0.9)
    vm_schedule = momentum_schedule(0.999)
    learner = adam(model.parameters, lr_schedule, momentum=m_schedule, 
                                                  variance_momentum=vm_schedule,
                                                  l1_regularization_weight=l1r,
                                                  l2_regularization_weight=l2r)
                    
    progress_writers = [ProgressPrinter(freq=10, tag='Training', log_to_file=log_dir, gen_heartbeat=gen_heartbeat), 
                        TrainingSummaryProgressCallback(5, lambda epoch, *unused_args: peek(model, input, output, epoch+1, network_type, tensorboard_writer, model_dir=output_dir, log_dir=log_dir))]
    
    tensorboard_writer = None
    if tensorboard_logdir is not None:
        tensorboard_writer = TensorBoardProgressWriter(freq=10, log_dir=tensorboard_logdir, model=model)
        progress_writers.append(tensorboard_writer)

    msg = "Start training: network({}), minibatch_size({}), max_epochs({}), unit_type({})\n".format(network_type, minibatch_size, max_epochs, unit_type)
    print(msg)
    with open(log_dir, "w") as text_file:
        text_file.write(msg)

    log_number_of_parameters(model) ; print()

    # perform model training
    if profiler_dir:
        start_profiler(profiler_dir, True)
    progress = criterion.train((input, output),
                               minibatch_size=minibatch_size, max_epochs=max_epochs, epoch_size=epoch_size,
                               parameter_learners=[learner],
                               callbacks=progress_writers)
    if profiler_dir:
        stop_profiler()

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--network', help='Network type, linear or cubic', required=False, default='linear')
    parser.add_argument('-e', '--epochs', help='total epochs', required=False, default='20')
    parser.add_argument('-profilerdir', '--profilerdir', help='directory for saving profiler output', required=False, default=None)
    parser.add_argument('-tensorboard_logdir', '--tensorboard_logdir', help='Directory where TensorBoard logs should be created', required=False, default='tblog')
    parser.add_argument('-basedir', '--basedir', help='Base Directory where the results is located', required=False, default='result')
    parser.add_argument('-outputdir', '--outputdir', help='Output directory for checkpoints and models', required=False, default='model')
    parser.add_argument('-logdir', '--logdir', help='Log file', required=False, default='log')
    parser.add_argument('-genheartbeat', '--genheartbeat', help="Turn on heart-beat for philly", action='store_true', default=False)
    parser.add_argument('-l1', '--l1regularization', help="L1 Regualarization weight", default=0.0)
    parser.add_argument('-l2', '--l2regularization', help="L2 Regualarization weight", default=0.0)
    parser.add_argument('-m', '--minibatch_size', help="Minibatch Size", default=32)
    parser.add_argument('-ut', '--unit_type', help="Learning rate per type, minibatch or sample", default='minibatch')
    parser.add_argument('-lr', '--learning_rate', help="Learning rate", default=0.0001)
    parser.add_argument('-db', '--motion_db', help="Motion DB filepath", default='database.npz')

    args = vars(parser.parse_args())

    X, Y, P = load_data(args['motion_db'])
    print(X.shape, Y.shape, P.shape)

    P = np.clip(P * 4, 0., 3.99999)
    Xp = np.concatenate([X, P[...,np.newaxis]], axis=-1)
    
    print(Xp.shape, Y.shape)
    print(Xp)

    now = datetime.now()
    set_dir = lambda bdir, tdir: '{}/{}'.format(bdir, tdir) if tdir is not None else None

    # setup directory 
    base_dir = '{}/{}'.format(args['basedir'], now.strftime('%Y-%m-%d_%H-%M-%S'))
    rawdir = [args['profilerdir'], args['outputdir'], args['logdir'], args['tensorboard_logdir']]
    profiler_dir, output_dir, log_dir, tensorboard_logdir = [set_dir(base_dir, tdir) for tdir in rawdir]
    print(profiler_dir, output_dir, log_dir, tensorboard_logdir)

    if base_dir is not None:
        os.makedirs(base_dir, exist_ok=True)
    if profiler_dir is not None:
        os.makedirs(profiler_dir, exist_ok=True)

    train(Xp, Y, network_type=args['network'],
                 minibatch_size=int(args['minibatch_size']),
                 unit_type=UnitType.sample if args['unit_type'] == 'sample' else UnitType.minibatch,
                 learning_rate=float(args['learning_rate']),
                 max_epochs=int(args['epochs']),
                 l1r=float(args['l1regularization']),
                 l2r=float(args['l1regularization']),
                 profiler_dir=profiler_dir,
                 log_dir=log_dir,
                 output_dir=output_dir,
                 tensorboard_logdir=tensorboard_logdir)