import cntk as C
import numpy as np

from cntk import Constant
from cntk.learners import adadelta, learning_rate_schedule, UnitType
from cntk.layers import Dense, Sequential, For, Dropout, Label, Tensor
from cntk.ops import relu, elu
from cntk.default_options import is_default_override, get_default_override, default_override_or
from cntk.layers.blocks import _initializer_for, _INFERRED, identity, UntestedBranchError  # helpers
from cntk.debugging import debug_model
from cntk.logging import TensorBoardProgressWriter
from cntk.internal import _as_tuple
from cntk.default_options import is_default_override, get_default_override, default_override_or
from cntk.variables import Parameter, Record, Constant
from cntk.ops.functions import Function, BlockFunction
from cntk.layers import *
from cntk.layers.sequence import *
from cntk.layers.models.attention import *
from cntk.layers.typing import *

#X = np.matrix('0 0 0; 0 1 0; 1 0 0; 1 1 0; 0 0 1; 0 1 1; 1 0 1; 1 1 1; 0 0 2; 0 1 2; 1 0 2; 1 1 2; 0 0 3; 0 1 3; 1 0 3; 1 1 3', dtype=np.float32)
X = np.matrix('0 0 0 1; 0 1 0 1; 1 0 0 1; 1 1 0 1; 0 0 1 2; 0 1 1 2; 1 0 1 2; 1 1 1 2; 0 0 2 3; 0 1 2 3; 1 0 2 3; 1 1 2 3; 0 0 3 0; 0 1 3 0; 1 0 3 0; 1 1 3 0', dtype=np.float32)
Y = np.matrix('0; 1; 1; 0; 1; 0; 0; 1; 1; 1; 0; 0; 0; 0; 1; 1', dtype=np.float32)

input_dim = 2
output_dim = 1
num_hiddel_layer = 1
hidden_layers_dim = 32
num_epochs = 100
slices = 4

if __name__=='__main__':
    input_val = C.input_variable(input_dim+2, np.float32)
    label_val = C.input_variable(output_dim, np.float32)

    activator = relu

    W1 = C.Parameter((slices,input_dim,hidden_layers_dim), init=C.glorot_normal(), name='W1')
    b1 = C.Parameter((slices,hidden_layers_dim), init=0, name='b1')

    W2 = C.Parameter((slices,hidden_layers_dim,hidden_layers_dim), init=C.glorot_normal(), name='W2')
    b2 = C.Parameter((slices,hidden_layers_dim), init=0, name='b2')

    W3 = C.Parameter((slices,hidden_layers_dim,output_dim), init=C.glorot_normal(), name='W3')
    b3 = C.Parameter((slices,output_dim), init=0, name='b3')

    x = input_val[:-2]
    p1 = input_val[-2]
    p2 = input_val[-1]

    W11 = C.gather(W1, p1)
    b11 = C.gather(b1, p1)
    W1x = C.reshape(W11, (input_dim,hidden_layers_dim))
    b1x = C.reshape(b11, (hidden_layers_dim,))

    W21 = C.gather(W2, p1)
    b21 = C.gather(b2, p1)
    W2x = C.reshape(W21, (hidden_layers_dim,hidden_layers_dim))
    b2x = C.reshape(b21, (hidden_layers_dim,))

    W31 = C.gather(W3, p1)
    b31 = C.gather(b3, p1)
    W3x = C.reshape(W31, (hidden_layers_dim,output_dim))
    b3x = C.reshape(b31, (output_dim,))

    x = activator(C.times(x, W1x) + b1x)
    x = activator(C.times(x, W2x) + b2x)
    x = C.times(x, W3x) + b3x

    model = x

    loss = C.losses.squared_error(model, label_val)
    metric = np.square(model(input_val) - label_val)
    #metric = C.metrics.classification_error(model, label)
    #criterion = C.combine([loss, metric])
    criterion = loss
    progress_writer = [C.logging.ProgressPrinter(
        tag = "Training",
        num_epochs = num_epochs
    )]
    tensorboard_writer = TensorBoardProgressWriter(freq=10, log_dir='log', model=model)

    #lr = learning_rate_schedule(1, UnitType.sample)
    lr = learning_rate_schedule(0.1, UnitType.minibatch)
    learner = C.learners.sgd(model.parameters, lr)
    trainer = C.Trainer(model, criterion, [learner], progress_writers=progress_writer)

    for i in range(num_epochs):
        trainer.train_minibatch({input_val: X, label_val: Y})#, phase: np.full((1,), 1.)})
        trainer.summarize_training_progress()
        if i % 20 == 0:
            print(model.eval(X))

    print('xor\n', np.around(model.eval(X)))
